package xjia;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Forwarder extends Thread {
	private InputStream in;
	private OutputStream out;

	public Forwarder(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
	}

	public void run() {
		byte[] buffer = new byte[4096];
		int length = -1;
		try {
			while ((length = in.read(buffer)) != -1) {
				out.write(buffer, 0, length);
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}
}
