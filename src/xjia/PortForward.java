package xjia;

import java.net.InetSocketAddress;
import java.net.ServerSocket;

public class PortForward {
	public static void main(String[] args) throws Exception {
		if (args.length < 4) {
			System.out.println("Usage: [myHost] [myPort] [itsHost] [itsPort]");
			return;
		}

		String myHost = args[0];
		int myPort = Integer.valueOf(args[1]);
		String itsHost = args[2];
		int itsPort = Integer.valueOf(args[3]);

		try (ServerSocket serverSocket = new ServerSocket()) {
			serverSocket.bind(new InetSocketAddress(myHost, myPort));
			System.out.println(serverSocket.getLocalSocketAddress());
			System.out.println("Forwarding to " + itsHost + ":" + itsPort);
			while (true) {
				new Worker(itsHost, itsPort, serverSocket.accept()).start();
			}
		}
	}
}
