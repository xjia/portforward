package xjia;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Worker extends Thread {
	private Socket targetSocket;
	private OutputStream targetInput;
	private InputStream targetOutput;

	private Socket sourceSocket;
	private InputStream sourceInput;
	private OutputStream sourceOutput;

	public Worker(String host, int port, Socket socket) throws Exception {
		targetSocket = new Socket(host, port);
		targetInput = targetSocket.getOutputStream();
		targetOutput = targetSocket.getInputStream();

		sourceSocket = socket;
		sourceInput = sourceSocket.getInputStream();
		sourceOutput = sourceSocket.getOutputStream();
	}

	public void run() {
		System.out.println("New connection from " + sourceSocket.getRemoteSocketAddress());
		new Forwarder(sourceInput, targetInput).start();
		new Forwarder(targetOutput, sourceOutput).start();
	}
}
