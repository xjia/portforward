PortForward
===========

Simple port forwarder implemented in Java

Usage: `java -jar PortForward.jar 202.120.38.146 3690 202.120.38.157 3690`

This is much simpler than iptables and/or socat, does not require root privilege, and works out of the box.

UPDATE: I wasn't aware of [simpleproxy](http://manpages.ubuntu.com/manpages/precise/man1/simpleproxy.1.html) when I was writing this tool.  Now I use simpleproxy because it doesn't require a Java runtime installed.
